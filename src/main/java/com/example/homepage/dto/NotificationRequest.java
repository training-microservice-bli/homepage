package com.example.homepage.dto;

import lombok.Data;

@Data
public class NotificationRequest {
    private String email;
    private String hp;
    private String judul;
    private String isi;
    private String jenisNotifikasi = "notifikasi-customer";
}

