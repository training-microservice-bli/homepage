package com.example.homepage.service;

import com.example.homepage.dto.NotificationRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducerService.class);

    @Autowired private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired private ObjectMapper objectMapper;

    @Value("${kafka.topic.notifikasi.request}")
    private String topikNotifikasi;

    public void kirimNotifikasi(NotificationRequest request) {

        try {
            String jsonNotifikasi = objectMapper.writeValueAsString(request);
            LOGGER.debug("JSON Notifikasi : {}", jsonNotifikasi);
            kafkaTemplate.send(topikNotifikasi, jsonNotifikasi);
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage(), e);
        }

    }
}
