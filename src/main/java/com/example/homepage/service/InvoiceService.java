package com.example.homepage.service;

import com.example.homepage.dto.Customer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

//@FeignClient("invoice")
@FeignClient(name = "invoice", fallback = InvoiceServiceFallback.class)
public interface InvoiceService {

    @GetMapping("/customer/")
    public Iterable<Customer> ambilDataCustomer();
}

