package com.example.homepage.service;

import com.example.homepage.dto.Customer;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Profile("default")
@Service
public class InvoiceServiceDummy {
    public Iterable<Customer> ambilDataCustomer(){
        List<Customer> dataDummy = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Customer c = new Customer();
            c.setId("id"+i);
            c.setNumber("D-00"+i);
            c.setName("Dummy Data 00"+i);
            c.setEmail(i+"@coba.com");
            c.setMobilePhone("0812223344"+i);
            dataDummy.add(c);
        }
        return dataDummy;
    }
}

